// Copyright (c) 2016, Fayez Qandeel and contributors
// For license information, please see license.txt

frappe.intervals_manager_dirottamenti = false;
$(window).on('hashchange', function() {
    frappe.intervals_manager_dirottamenti.clearAll();
    //console.log(frappe.intervals_manager);
    if (window.location.hash.indexOf("Dirottamenti%20Template/") == -1 && window.location.hash.indexOf("Dirottamenti Template/") == -1) {
        $(".layout-side-section").show().next().removeClass("col-md-12").addClass("col-md-10");
        window.full_screen_mode = false;
        $('.container').removeClass("ravenna_container");
        $('.fullscreen_btn_').text("Full Screen");
        frm.page.wrapper.find('.menu-btn-group').show();


    } else {

        frappe.intervals_manager_dirottamenti.rebuild();
        frm.page.wrapper.find('.menu-btn-group').hide();
        $(".layout-side-section").hide().next().removeClass("col-md-10").addClass("col-md-12");
    }
});

frappe.ui.form.on('Dirottamenti Template', {
    setup: function(frm) {


        frm.page.clear_inner_toolbar();
        frappe.intervals_manager_dirottamenti = new frappe.intervals_manager_dirottamenti(frm);
        $(function() {
            frm.page.wrapper.find('.menu-btn-group').hide()
            $(".layout-side-section").hide().next().removeClass("col-md-10").addClass("col-md-12");
            if ($('.fullscreen_btn_').length == 0) {
                $('.page-actions').find('.menu-btn-group').before('<span style="margin-right:5px;" class="fullscreen_btn_ btn btn-secondary btn-default btn-sm">Full Screen</span>');
            }
            window.full_screen_mode = false;
            $('.fullscreen_btn_').on('click', function() {


                if (!window.full_screen_mode) {
                    $(this).text("Close Full Screen");
                    window.full_screen_mode = true;
                } else {
                    $(this).text("Full Screen");
                    window.full_screen_mode = false;
                }
                $('.container').toggleClass("dirottamenti_container");
            });
        });
    },
    validate:function(frm){
        frm.timeline.insert_comment("Workflow", "- saved the template");
        frm.timeline.refresh();
    },

    refresh: function(frm) {

        var me = this;
        me.frm = frm;

        if (frm.items_editor_dirottamenti) {

            // frm.page.set_secondary_action(__("Create Order"), function() {
            //     frappe.call({
            //         method: 'ravenna_sales_order.ravenna_sales_order.doctype.sales_order_template.sales_order_template.create_sales_order',
            //         args: {
            //             template: me.frm.doc.name
            //         },
            //         callback: function(r) {
            //             if (r.message) {
            //                 msgprint(__("Sales orders successfully created check it out!"));
            //                 me.frm.set_value('template_status', 'Sales Order');
            //                 refresh_field("template_status");
            //                 me.frm.save();
            //             }
            //         }
            //     });
						//
            // });

        }
    },
    onload: function(frm) {
        var me = this;
        me.frm = frm;




        // frm.page.set_secondary_action(__("Create Order"), function() {
        //     frappe.call({
        //         method: 'ravenna_sales_order.ravenna_sales_order.doctype.sales_order_template.sales_order_template.create_sales_order',
        //         args: {
        //             template: me.frm.doc.name
        //         },
        //         callback: function(r) {
        //             if (r.message) {
        //                 msgprint(__("Sales orders successfully created check it out!"));
        //                 me.frm.set_value('template_status', 'Sales Order');
        //                 refresh_field("template_status");
        //                 me.frm.save();
        //             }
        //         }
        //     });
				//
        // });
        if (!frm.items_editor_dirottamenti) {
						console.log(frm.fields_dict);
            frm.items_editor_dirottamenti = new frappe.ItemsEditor_dirottamenti(frm, frm.fields_dict.dirottamenti_template_details_html.wrapper);
        }
        if (frm.items_editor_dirottamenti && frm.doc.ravenna_sales_order && frm.doc.ravenna_sales_order != "") {
            frappe.call({
                method: 'dirottamenti.dirottamenti.doctype.dirottamenti_template.dirottamenti_template.get_template',
                args: {
                    template: frm.doc.ravenna_sales_order
                },
                callback: function(r) {
                    if (r.message && r.message.template_custom_items  && r.message.template_custom_items.length > 0) {
                        frm.items_editor_dirottamenti.main_template = r.message;
                        $(document).trigger("main_template_updated");
                    }
                }
            });
        }
    },
    ravenna_sales_order: function(frm) {

        frappe.call({
            method: 'dirottamenti.dirottamenti.doctype.dirottamenti_template.dirottamenti_template.get_template',
            args: {
                template: frm.doc.ravenna_sales_order
            },
            callback: function(r) {
							if (r.message && r.message.template_custom_items  && r.message.template_custom_items.length > 0) {
									frm.items_editor_dirottamenti.main_template = r.message;
									$(frm.items_editor_dirottamenti.root_area).removeClass("withoutScroll").empty();
									$(document).trigger("main_template_updated");
							}else {
                    //console.log($(frm.items_editor.root_area));
                    $(frm.items_editor_dirottamenti.root_area).addClass("withoutScroll").empty().text('No customers found on the template...');
              }

            }
        });
    }
});
frappe.intervals_manager_dirottamenti = Class.extend({
    init: function(frm) {

        this.frm = frm;
        var me = this;
        if (!this.intervals_array) {
            this.intervals_array = setInterval(function() {
                if (me.frm.doctype == "Dirottamenti Template") {
                    me.frm.save();
                    me.frm.timeline.insert_comment("Workflow", "- auto saved the template");
                    me.frm.timeline.refresh();
                }
            }, 60000);
        }
    },
    rebuild: function() {
        var me = this;
        if (!this.intervals_array) {
            this.intervals_array = setInterval(function() {
                if (me.frm.doctype == "Dirottamenti Template") {
                    me.frm.save();
                    me.frm.timeline.insert_comment("Workflow", "- auto saved the template");
                    me.frm.timeline.refresh();
                }
            }, 60000);
        }
    },
    clearAll: function() {
        clearInterval(this.intervals_array);
        this.intervals_array = false;
    }
});
frappe.ItemsEditor_dirottamenti = Class.extend({
    init: function(frm, customers_area_root) {
        // this.party_field = frappe.ui.form.make_control({
        //     df: {
        //         "fieldtype": "Link",
        //         "options": "Customer",
        //         "label": "select customer",
        //         "fieldname": "customer_field_name",
        //         "placeholder": __("Select or add new customer")
        //     },
        //     parent: customers_area_root,
        //     only_input: true,
        // });

        // this.party_field.make_input();

        this.frm = frm;
        this.root_area = customers_area_root;
        var me = this;
        $(document).on("main_template_updated", function() {

            $(customers_area_root).empty();
            var customers_area = $('<div class="form-grid"><div class="grid-heading-row"><div class="col grid-static-col col-xs-2 text-center" ><div class="static-area ellipsis">Customer</div></div></div><div class="grid-body"><div style="background:#fff" class="items-area"></div></div></div>')
                .appendTo(customers_area_root);
						console.log(me.frm);
            $(customers_area_root).attr("id", "dirottamenti_template_details_html");
            me.wrapper = customers_area;
            me.prepare();
        });
    },
    rebuild: function() {
        customers_area.find('.items-area').empty();
        customers_area.find('.grid-heading-row .grid-row .col:not(:first)').remove();
        this.prepare();
    },
		prepare:function(){
			var me=this;
			me.customers = {};
			me.items_list = [];

			for (var i in me.main_template.template_custom_items) {
				if(me.items_list.indexOf(me.main_template.template_custom_items[i].item)==-1){
					me.items_list.push(me.main_template.template_custom_items[i].item);
				}
				if(me.customers[me.main_template.template_custom_items[i].customer]){
					me.customers[me.main_template.template_custom_items[i].customer].push(me.main_template.template_custom_items[i]);
				}else{
					me.customers[me.main_template.template_custom_items[i].customer] = [me.main_template.template_custom_items[i]];
				}

			}
			this.make();
		},
    make: function() {
        var me = this;
        if (!me.main_template) {
            msgprint(__("Your Ravenna template is empty"));
        }
        for (var i in me.items_list) {
            me.wrapper.find(".grid-heading-row").append('<div class="col grid-static-col col-xs-2  text-center "><div class="static-area ellipsis">' + me.items_list[i] + '</div></div>');
        }
        me.wrapper.find(".grid-heading-row ").append('<div class="col grid-static-col col-xs-2  text-center "><div class="static-area ellipsis">Dirotta</div></div><div style="clear:both"></div>');



        var items_obj = {};
				var items_obj_new = {};
        for (var i in me.main_template.template_custom_items) {
            var item = me.main_template.template_custom_items[i];
            items_obj[item.customer + item.item] = item.quantity || 0;
        }

				for (var i in me.frm.doc.dirottamenti_template_details) {
            var item = me.frm.doc.dirottamenti_template_details[i];
            items_obj_new[item.customer + item.item] = item.quantity;
        }

				this.items_obj_new = items_obj_new;

        for (var i in me.customers) {
            var customer_html = '<div class="col grid-static-col col-xs-2 text-center" style="padding:5px 0px 5px 0px !important;"><div class="static-area ellipsis">' + i + '</div></div>';
            for (var j in me.customers[i]) {

                var quantity = items_obj_new[i + me.customers[i][j].item] || items_obj[i + me.customers[i][j].item];
                customer_html += '<div class="col grid-static-col col-xs-2 text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" data-fieldtype="Int" data-fieldname="cost_shipping" title="cost_shipping"><input data-item="' + me.customers[i][j]["item"] + '" data-customer="' + i + '" type="text" autocomplete="off" class="input-with-feedback form-control bold input-sm customer-item text-center item-withvalue"  value="' + quantity + '"  placeholder="0" data-doctype="Sales Order Template Item" data-col-idx="1"></div></div><div class="static-area ellipsis"></div></div>';
            }
						  customer_html += '<div class="col grid-static-col col-xs-2 text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" ><a href="" class ="btn btn-secondary btn-default btn-sm show_customer" data-customer="'+i+'" style="    vertical-align: middle;display: block;border-radius: 0px;">show</a></div></div><div class="static-area ellipsis"></div></div>';

            me.wrapper.find(".items-area").append('<div class="editable_row">' + customer_html + '<div style="clear:both"></div></div>');
        }
                me.bind();

    },
    bind: function() {
        var me = this;
        this.wrapper.on("change", ".customer-item", function() {
            me.apply_change(this);
        });

				this.wrapper.on("click", ".show_customer", function(e) {
					var customer = $(this).attr("data-customer");
					e.preventDefault();


					var html = '<div style="margin-top:20px" class="row"><div class="col-md-6"><b>Ordered</b>';

					for(var i in me.customers[customer]){
						html +='<div>'+me.customers[customer][i].item+' = '+me.customers[customer][i].quantity+'</div>';
					}
					var tipo="";
          var target_customer = "";
					html +='</div><div class="col-md-6"><b>Dirotta</b>';
					for(var i in me.frm.doc.dirottamenti_template_details){
						var item = me.frm.doc.dirottamenti_template_details[i];
						if(customer == item.customer){
							html +='<div>'+item.item+' = '+me.items_obj_new[item.customer+item.item]+'</div>';
							tipo = item.tipo||"";
              target_customer =item.target_customer||"";
						}
					}

					html +='</div></div>';

					var d = new frappe.ui.Dialog({
						title: __("Dirottamenti"),
						fields: [
              {
								fieldtype:"HTML",
								options: '<div> <b>Current Customer</b> </div><div style="margin-bottom:-20px">'+customer+'<br/><br/><div><b>Target customer</b> </div></div>',
								fieldname:"html"
							},
							{
								fieldtype:"Link", fieldname:"customer",options:"Customer"
							},
							{
								fieldtype:"HTML",
								options: html,
								fieldname:"html"
							},
							{
								fieldtype:"Select",
								options: "Totale\nParziale\nReso",
								fieldname:"tipo",
								default:"Totale"
							}

						]
					});

					d.set_value("customer",target_customer);
					d.set_value("tipo",tipo);

					d.get_input("tipo").change(function() {
						var args = d.get_values();
						var selected_customer = args.customer;
						for(var i in me.frm.doc.dirottamenti_template_details){
							var item = me.frm.doc.dirottamenti_template_details[i];
							if(selected_customer == item.customer){
								item.tipo = args.tipo||'';
							}
						}
						refresh_field("dirottamenti_template_details");
					});
					d.get_input("customer").change(function() {

						var args = d.get_values();
          //  me.frm.doc.target_customer = args.customer;


            for(var i in me.frm.doc.dirottamenti_template_details){
              if(customer==me.frm.doc.dirottamenti_template_details[i].customer){
                me.frm.doc.dirottamenti_template_details[i].target_customer = args.customer;
              }
            }
            console.log(me.frm.doc);
            me.frm.refresh_field("dirottamenti_template_details");
            // args.frm = me.frm;
            // console.log(args);
            // frappe.realtime.publish("dirottamenti_update",args);
						// var selected_customer = args.customer;
						// var html = '<div class="row"><div class="col-md-6"><b>Ordered</b>';
						// var tipo = "";
						// for(var i in me.customers[selected_customer]){
						// 	html +='<div>'+me.customers[selected_customer][i].item+' = '+me.customers[selected_customer][i].quantity+'</div>';
						// }
						// html +='</div><div class="col-md-6"><b>Dirotta</b>';
						// for(var i in me.frm.doc.dirottamenti_template_details){
						// 	var item = me.frm.doc.dirottamenti_template_details[i];
						// 	if(selected_customer == item.customer){
						// 		tipo = item.tipo||'';
						// 		html +='<div>'+item.item+' = '+me.items_obj_new[item.customer+item.item]+'</div>';
						// 	}
						// }
            //
						// html +='</div></div>';
						// d.fields_dict.html.wrapper.innerHTML = html;
						// d.set_value("tip",tipo);
					});
					d.show();
        });




    },
    apply_change: function(ele) {
        var me = this;


        var customer = $(ele).parents('.editable_row:first').find('.item-withvalue:first').attr("data-customer");
        $(ele).parents('.editable_row:first').find('.item-withvalue').each(function() {
            var found = false;
            var item = $(this).attr('data-item');

            var quantity = $(this).val() || 0;
            for (var i in me.frm.doc.dirottamenti_template_details) {
                var customer_item = me.frm.doc.dirottamenti_template_details[i];
                if (customer_item.customer == customer && customer_item.item == item) {
                    customer_item.quantity = quantity;
                    found = true;
                }
            }

            if (!found) {
                var customer_item = frappe.model.add_child(me.frm.doc, "Dirottamenti Template Details", "dirottamenti_template_details");
                customer_item.customer = customer;
                customer_item.item = item;
                customer_item.quantity = quantity;
            }
						var items_obj_new = {};

						for (var i in me.frm.doc.dirottamenti_template_details) {
		            var item = me.frm.doc.dirottamenti_template_details[i];
		            items_obj_new[item.customer + item.item] = item.quantity;
		        }

						me.items_obj_new = items_obj_new;

            refresh_field("dirottamenti_template_details");
        });
    }
});
