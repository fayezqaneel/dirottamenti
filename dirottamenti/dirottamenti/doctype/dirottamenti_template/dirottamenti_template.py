# -*- coding: utf-8 -*-
# Copyright (c) 2015, Fayez Qandeel and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.desk.form.utils import add_comment
class DirottamentiTemplate(Document):
	def __iter__(self):
		iters = dict((x,y) for x,y in self.__dict__.items() if x[:2] != '__')
		iters.update(self.__dict__)
		for x,y in iters.items():
			yield x,y
	def on_update(self):
		data = dict(self)
		data["user"] = frappe.session.user
		frappe.publish_realtime("dirottamenti_updates", data, after_commit=True)
		trasporti = frappe.get_doc("Trasporti Template",{"ravenna_sales_order":self.ravenna_sales_order})
		origin = frappe.get_doc("Sales Order Template",{"name":self.ravenna_sales_order})
		modified = {}
		for item in origin.template_custom_items:
			for subitem in self.dirottamenti_template_details:
				if item.customer == subitem.customer and item.item==subitem.item and item.quantity != subitem.quantity:
					modified[item.customer] = self.name
		for item in trasporti.trasporti_template_details:
			if item.customer in modified:
				item.dirottato  = modified[item.customer]
		doc = frappe.get_doc({
			"doctype": "Communication",
			"communication_type": "Comment",
			"comment_type": "Workflow",
			"reference_doctype": "Trasporti Template",
			"reference_name": trasporti.name,
			"content": " - "+frappe.session.user+" did live changes on trasporti values",
			"sender": frappe.session.user
		})
		doc.insert(ignore_permissions=True)
		trasporti.save()
		changes = {}
		changes_count = 0
		for item in self.dirottamenti_template_details:
			for subitem in origin.template_custom_items:
				if item.customer==subitem.customer and item.item==subitem.item and item.quantity!=subitem.quantity:
					change = {}
					change["old_qty"] = subitem.quantity
					change["new_qty"] = item.quantity
					change["customer"] = item.customer
					change["item"] = item.item
					if item.customer in changes:
						changes[item.customer].append(change)
					else:
						changes[item.customer]=[change];
					subitem.quantity = item.quantity
					changes_count +=1

			changes_string = ""
		for i, value in changes.iteritems():
			customer = i
			changes_string +=" customer="+str(i)+","
			for item in value:
				changes_string += " item="+str(item["item"])
				changes_string += " "+str(item["old_qty"])+"=>"+str(item["new_qty"])+","
		if changes_count>0:
			doc = frappe.get_doc({
				"doctype": "Communication",
				"communication_type": "Comment",
				"comment_type": "Workflow",
				"reference_doctype": "Sales Order Template",
				"reference_name": origin.name,
				"content": " - "+frappe.session.user+" changed "+changes_string,
				"sender": frappe.session.user
			})
			doc.insert(ignore_permissions=True)
			changes_count = 0
			origin.save()

@frappe.whitelist()
def get_template(template=None):
	template = frappe.get_doc("Sales Order Template",template)
	customers = frappe.get_list("Customer",filters={"customer_group":template.customer_group},fields=["name"])
	items_ids = []
	items = []
	for customer in customers:
		items_ids.append(customer.name)

	for template_item in template.template_custom_items:
		if template_item.customer in items_ids:
			items.append(template_item)

	template.template_custom_items = items
	return template
