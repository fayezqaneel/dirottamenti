import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  '#dirottamenti_template_details_html form-grid': {
    display: 'inline-block!important'
  },
  '#dirottamenti_template_details_html': {
    overflowX: 'scroll',
    overflowY: 'auto',
    width: [{ unit: '%H', value: 1 }],
    paddingBottom: [{ unit: 'px', value: 20 }],
    marginTop: [{ unit: 'px', value: 20 }, { unit: 'string', value: '!important' }]
  },
  '#dirottamenti_template_details_htmlwithoutScroll': {
    overflowX: 'visible !important'
  },
  '#dirottamenti_template_details_html col-xs-2': {
    display: 'table-cell !important',
    width: [{ unit: 'px', value: 150 }],
    float: 'none'
  },
  '#dirottamenti_template_details_html grid-heading-row': {
    display: 'flex !important'
  },
  dirottamenti_container: {
    width: [{ unit: '%H', value: 1 }, { unit: 'string', value: '!important' }]
  },
  '#dirottamenti_template_details_html editable_row': {
    borderBottom: [{ unit: 'px', value: 1 }, { unit: 'string', value: 'solid' }, { unit: 'string', value: '#d1d8dd' }]
  },
  '#dirottamenti_template_details_html editable_row frappe-control': {
    margin: [{ unit: 'px', value: 0 }, { unit: 'px', value: 0 }, { unit: 'px', value: 0 }, { unit: 'px', value: 0 }]
  },
  '#dirottamenti_template_details_html editable_row grid-static-col': {
    padding: [{ unit: 'px', value: 0 }, { unit: 'px', value: 0 }, { unit: 'px', value: 0 }, { unit: 'px', value: 0 }]
  },
  '#dirottamenti_template_details_html editable_row grid-static-col': {
    height: [{ unit: 'string', value: 'auto' }],
    float: 'left'
  },
  '#dirottamenti_template_details_html editable_row form-control': {
    border: [{ unit: 'string', value: 'none' }]
  }
});
